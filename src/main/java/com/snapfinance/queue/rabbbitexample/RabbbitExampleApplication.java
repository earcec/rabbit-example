package com.snapfinance.queue.rabbbitexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbbitExampleApplication {

  public static void main(String[] args) {
    SpringApplication.run(RabbbitExampleApplication.class, args);
  }
}
