package com.snapfinance.queue.rabbbitexample.service;

import com.snapfinance.queue.rabbbitexample.dto.BodyRequest;
import com.snapfinance.queue.rabbbitexample.dto.BodyResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RabbitSenderService {

  @Autowired private RabbitTemplate rabbitTemplate;

  @Autowired private Queue queue;

  public BodyResponse send(BodyRequest bodyRequest) {
    rabbitTemplate.convertAndSend(queue.getName(), bodyRequest.getMessage());
    log.info("Sent '{}'", bodyRequest.getMessage());
    return new BodyResponse(true, "Message put");
  }
}
