package com.snapfinance.queue.rabbbitexample.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Slf4j
@Component
public class RabbitReceiverListener implements RabbitListenerConfigurer {

  @Override
  public void configureRabbitListeners(
      RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {
    log.info("configureRabbitListeners");
  }

  @RabbitListener(queues = "${rabbit.queue.name}")
  public void receivedMessage(String message) {
    StopWatch watch = new StopWatch();

    watch.start();

    log.info("Received '{}'", message);
    watch.stop();
    log.info("Process message in {} seconds", watch.getTotalTimeSeconds());
  }
}
