package com.snapfinance.queue.rabbbitexample.controller;

import com.snapfinance.queue.rabbbitexample.dto.BodyRequest;
import com.snapfinance.queue.rabbbitexample.dto.BodyResponse;
import com.snapfinance.queue.rabbbitexample.service.RabbitSenderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/message")
public class SendMessageController {

  @Autowired private RabbitSenderService rabbitSenderService;

  @Operation(summary = "Put message in queue")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Message sent",
            content = {@Content(mediaType = "application/json")}),
        @ApiResponse(
            responseCode = "500",
            description = " Internal Server Error",
            content = @Content)
      })
  @PostMapping
  public ResponseEntity<BodyResponse> addQueueMessage(@RequestBody BodyRequest bodyRequest) {
    return ResponseEntity.ok(rabbitSenderService.send(bodyRequest));
  }
}
