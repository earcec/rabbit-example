package com.snapfinance.queue.rabbbitexample.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BodyResponse implements Serializable {

  @NonNull private boolean result;

  @NonNull private String message;
}
